Instructions to run Into The Oak Tree:

	Importing the Repository From Bitbucket:

		Step 1: Copy the URI from the team_oak_project repository.
		
		Step 2: Open eclipse and import the repository. Import > Projects from Git > Clone URI > Paste URI and enter bitbucket credentials.

		Step 3: The user must have referenced files: jackson-annotations- 2.12.2.jar, jackson-core-2.12.2.jar, jackson-databind-2.12.2.jar,
			sqlite-jdbc-3.32.3.2.jar, and Story2.json. The resources file must contain: "Into the Oak Tree (Title Screen Pic).png" and "PlayerInfo.db"

			!>>>>> These have been included in the repository for the user, so the user doesn't have to add them. If for some reason, they do not carry over, the user
	       			must reference them manually.<<<<<<<<!

		Step 4: To add the .JAR files as referenced libraries: Right click on OakTreeProject > Build Path > Configure Build Path > While under the "Libraries" tab, click Classpath > 
			Add JARs > locate the jar files at the bottom of the project > ADD EVERY .JAR FILE > Click apply and then apply and close.
	
			!>>>>> For this step only add jackson-annotations-2.12.2.jar, jackson-core-2.12.2.jar, jackson-databind-2.12.2.jar, and sqlite-jdbc-3.32.3.2.jar.<<<<<<<<!

	Steps for running application:

		Step 1: ALWAYS run Title Screen first for full effect.

		Step 2: Click "Play!" to start the game. "Player Info" lets users view adventures who completed their quest. "Credits" takes the user to credits. "Quit" exits the
			application.

		Step 3: Enter the character information, OR ELSE an error will occur. Once information is correctly entered, hit submit to start playing the game.

		Step 4: Click the buttons available at the bottom of the screen to progress through the story. CORRECT decisions result in further progression, 
			while WRONG choices result in a GAME OVER. CHOOSE WISLEY.

		Step 5: If an incorrect decision is chosen, it is GAME OVER and you must click "Quit" to exit the game. If you complete the game successfully, 
			then you will be taken to the player info screen where you will be entered into the database.

		Step 6: Once done at the player info screen, You will click "Main Menu" to be taken to the Title Screen to either play again or quit the application.


	Functionality: NO ERRORS

		Everything is functioning correctly in the program!!!!

   