package application;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.DriverManager;
import java.sql.SQLException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
/**
 * PlayerInfo
 * Created on April 8, 2021
 */
public class PlayerInfoController {
	
	/**
	 * Calls PlayerInfo.java to pull information from PlayerInfo constructor, updating PlayerInfoController as the database changes.
	 */
	private PlayerInfoModel model = new PlayerInfoModel();

	/**
	 * Returns user to Title screen.
	 */
    @FXML
    private Button mainMenuButton;

    /**
     * Holds the data retrieved from PlayerInfo.java. Houses columns (id, name, dateCompleted, role)
     */
    @FXML
    private TableView<PlayerInfo> tableView;

    /**
     * Returns IDs as shown in 'id' column in PlayerInfo.db
     */
    @FXML
    private TableColumn<PlayerInfo, String> idColumn;

    /**
     * Returns dates completed as shown in 'date' column in PlayerInfo.db
     */
    @FXML
    private TableColumn<PlayerInfo, String> dateCompletedColumn;

    /**
     * Returns names as shown in 'name' column in PlayerInfo.db
     */
    @FXML
    private TableColumn<PlayerInfo, String> nameColumn;
    
    /**
     * Returns roles, or genders/titles, as shown in 'role' column in PlayerInfo.db
     */
    @FXML
    private TableColumn<PlayerInfo, String> roleColumn;

    /**
     * If user clicks on  mainMenuButton button, TitleScreenProject.fxml is retrieved and loaded.
     * @param event
     * @throws IOException
     */
    @FXML
    void handleMainMenu(ActionEvent event) throws IOException {
    	FXMLLoader loader = new FXMLLoader();
    	loader.setLocation(getClass().getResource("TitleScreenProject.fxml"));
    	Parent parent = loader.load();
    	Scene scene = new Scene(parent);
    	Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
    	window.setScene(scene);
    	window.show();

    }
    /**
     * Sets up the 'id', 'dateCompleted', 'name' and 'role' columns.
     */
    @FXML
    public void initialize() {
    	idColumn.setCellValueFactory(new PropertyValueFactory<PlayerInfo, String>("id"));
    	dateCompletedColumn.setCellValueFactory(new PropertyValueFactory<PlayerInfo, String>("dateCompleted"));
    	nameColumn.setCellValueFactory(new PropertyValueFactory<PlayerInfo, String>("name"));
    	roleColumn.setCellValueFactory(new PropertyValueFactory<PlayerInfo, String>("role"));
    	tableView.setItems(model.getPlayerInfo());
    }
}

