package application;

import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
/**
 * TitleScreenController
 * Created on March 25, 2021
 *
 */
public class TitleScreenController {

	/**
	 * Begins game by taking user to CharCreate.
	 */
    @FXML
    private Button playButton;

    /**
     * Takes user to database of winners listed under PlayerInfoScreen.
     */
    @FXML
    private Button playerInfoButton;

    /**
     * Takes user to CreditsScreen.
     */
    @FXML
    private Button creditsButton;

    /**
     * Quits the application and closes the application window.
     */
    @FXML
    private Button quitButton;

    /**
     * When user clicks 'PlayerInfo' button, PlayerInfo.fxml is retrieved and loaded
     * PlayerInfo.db is queried when user enters PlayerInfoScreen.
     * @param event
     * @throws IOException
     */
    @FXML
    void handlePlayerInfo(ActionEvent event) throws IOException {
    	FXMLLoader loader = new FXMLLoader();
    	loader.setLocation(getClass().getResource("PlayerInfo.fxml"));
    	Parent parent = loader.load();
    	Scene scene = new Scene(parent);
    	Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
    	window.setScene(scene);
    	window.show();

    }

    /**
     * When user clicks 'Credits' button, CreditsScreen.fxml is retrieved and loaded
     * @param event
     * @throws IOException
     */
    @FXML
    void handleCredits(ActionEvent event) throws IOException{
    	FXMLLoader loader = new FXMLLoader();
    	loader.setLocation(getClass().getResource("CreditsScreen.fxml"));
    	Parent parent = loader.load();
    	Scene scene = new Scene(parent);
    	Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
    	window.setScene(scene);
    	window.show();
    }

    /**
     * When user clicks 'Play' button, CharCreate.fxml is retrieved and loaded
     * @param event
     * @throws IOException
     */
    @FXML
    void handlePlay(ActionEvent event) throws IOException {
    	FXMLLoader loader = new FXMLLoader();
    	loader.setLocation(getClass().getResource("CharCreate.fxml"));
    	Parent parent = loader.load();
    	Scene scene = new Scene(parent);
    	Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
    	window.setScene(scene);
    	window.show();
    }

    /**
     * When user clicks 'Quit', the application halts and the application window is closed.
     * @param event
     */
    @FXML
    void handleQuit(ActionEvent event) {
    	System.exit(0);

    }

}

