package application;
	
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.fxml.FXMLLoader;

/**
 * GameScreen
 * Created on March 25, 2021
 */
public class GameScreen extends Application {
	/**
	 * Sets dimensions of application window.
	 * Retrieves GameScreen.fxml for styling and arrangement.
	 */
	@Override
	public void start(Stage primaryStage) {
		try {
			BorderPane root = (BorderPane)FXMLLoader.load(getClass().getResource("GameScreen.fxml"));
			Scene scene = new Scene(root,800,600);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * launches application starting at GameScreen screen.
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
	}
}
