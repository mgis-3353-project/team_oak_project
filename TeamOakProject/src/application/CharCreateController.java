package application;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.DriverManager;
import java.sql.SQLException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
/**
 * CharCreateController
 * Created on April 15, 2011
 */
public class CharCreateController {
	
	/**
	 * Gender dictates the title saved in the database.
	 * Masculine gender: Warrior / Feminine gender: Shieldmaiden
	 */
    private String gender;
    
    /**
     * Name of the character saved in the database if win condition met.
     */
    private String charName;

    /**
     * User input is saved from nameField after submit button is clicked.
     */
    @FXML
    private TextField nameField;

    /**
     * This button can be clicked to set title to 'Warrior'.
     * One of the two gender buttons must be pressed to proceed.
     */
    @FXML
    private Button mascButton;

    /**
     * This button can be clicked to set title to 'Shieldmaiden'.
     *  One of the two gender buttons must be pressed to proceed.
     */
    @FXML
    private Button femButton;

    /**
     * Saves gender and name to be added to PlayerInfo.db if win condition is triggered.
     */
    @FXML
    private Button submitNameButton;
    

    /**
     * Upon clicking the button labeled 'Shieldmaiden', gender is set to 'Shieldmaiden'.
     * gender is saved under PlayerInfo.db as 'Title'
     * @param event
     */
    @FXML
    void femEvent(ActionEvent event) {
    	gender = "Shieldmaiden";
    }

    /**
     * Upon clicking the button labeled 'Shieldmaiden', gender is set to 'Shieldmaiden'.
     * @param event
     */
    @FXML
    void mascEvent(ActionEvent event) {
    	gender = "Warrior";
    }
    
    /**
     * Clicking 'Submit' button reads user input from nameField, stores this in charName.
     * 'Submit' button also stores whichever button gender clicked last.
     * charName and gender are initialized and sent to GameScreenController.
     * IllegalArgumentException is thrown if user fails to enter a name. User is notified.
     * NullPointerException is thrown if user fails to select a gender. User is notified.
     * Retrieves and loads GameScreen.fxml to move to GameScreen.
     * @param event
     * @throws IOException
     */
    @FXML
    void submitCharEvent(ActionEvent event) throws IOException {
    	 
    	try {
    	charName = nameField.getText();
    	
    	if(nameField.getText().trim().equals("")) {
    		throw new IllegalArgumentException();
    		
    	}
    	
    	FXMLLoader loader = new FXMLLoader();
    	loader.setLocation(getClass().getResource("GameScreen.fxml"));
    	Parent parent = loader.load();
    	
    	GameScreenController controller = loader.getController();
    	controller.initData(gender, charName);
    	
    	Scene scene = new Scene(parent);
    	Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
    	window.setScene(scene);
    	window.show();
    	
    	}
    	
    	catch (NullPointerException e) {
    		Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("ERROR");
			alert.setHeaderText("NO TITLE SELECTED");
			alert.setContentText(String.format("You didn't select a title.", 
			e.getMessage().substring(e.getMessage().indexOf(":") + 2 )));
			alert.showAndWait();
    	}
    	
    	
    	catch (IllegalArgumentException e) {
    		Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("ERROR");
			alert.setHeaderText("NO NAME ENTERED");
			alert.setContentText("You didn't enter a name.");
			alert.showAndWait();
    	}
    
    }

}
