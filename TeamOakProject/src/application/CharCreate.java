package application;
	
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.fxml.FXMLLoader;
/**
 *CharCreate 
 *Created on April 10, 2021
 */
public class CharCreate extends Application {
	/**
	 * Sets dimensions of application window.
	 * Retrieves CharCreate.fxml for styling and arrangement.
	 */
	@Override
	public void start(Stage primaryStage) {
		try {
			AnchorPane root = (AnchorPane)FXMLLoader.load(getClass().getResource("CharCreate.fxml"));
			Scene scene = new Scene(root,400,400);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * launches application starting at CharCreate screen.
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
	}
}
