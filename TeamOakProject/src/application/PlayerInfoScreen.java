package application;
	
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.fxml.FXMLLoader;

/**
 * PlayerInfoScreen
 * Created on April 8, 2021
 */
public class PlayerInfoScreen extends Application {
	/**
	 * Sets dimensions of application window.
	 * Retrieves PlayerInfo.fxml for elements and styling of the screen and PlayerInfo.css for minor styling of the database.
	 */
	@Override
	public void start(Stage primaryStage) {
		try {
			AnchorPane root = (AnchorPane)FXMLLoader.load(getClass().getResource("PlayerInfo.fxml"));
			Scene scene = new Scene(root,800,600);
			scene.getStylesheets().add(getClass().getResource("PlayerInfo.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.setTitle("Player Info");
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Launches application starting at PlayerInfoScreen screen.
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
	}
}
