package application;

/**
 * Story
 * Created on April 5, 2021
 */
public class Story {
	/**
	 * Each id number holds a StoryText, AText, AID, BText, BID, CText, CID, DText, and DID with the corresponding number.
	 */
	public int id;
	
	/**
	 * The dynamicText TextFlow reads the StoryText ID and then displays the story and choices for the corresponding ID.
	 */
	public String StoryText;
	
	/**
	 * The dynamicText TextFlow reads the StoryText ID and then displays the text on the A button for the corresponding ID.
	 */
	public String AText;
	
	/**
	 * Directs to the Story object for that A ID.
	 */
	public int AID;
	
	/**
	 * The dynamicText TextFlow reads the StoryText ID and then displays the text on the B button for the corresponding ID.
	 */
	public String BText;
	
	/**
	 * Directs to the Story object for that B ID.
	 */
	public int BID;
	/**
	 * The dynamicText TextFlow reads the StoryText ID and then displays the text on the C button for the corresponding ID.
	 */
	public String CText;
	
	/**
	 * Directs to the Story object for that C ID.
	 */
	public int CID;
	/**
	 * The dynamicText TextFlow reads the StoryText ID and then displays the text on the D button for the corresponding ID.
	 */
	public String DText;
	
	/**
	 * Directs to the Story object for that D ID.
	 */
	public int DID;
	
	/**
	 * @return the Story ID
	 */
	public int getId() {
		return id;
	}
	/**
	 * Sets the Story ID
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * Gets the Story text.
	 * @return
	 */
	public String getStoryText() {
		return StoryText;
	}
	/**
	 * Sets the Story text.
	 * @param storyText
	 */
	public void setStoryText(String storyText) {
		StoryText = storyText;
	}
	
	/**
	 * Gets the A text for that ID.
	 * @return
	 */
	public String getAText() {
		return AText;
	}
	/**
	 * Sets the A text for that ID.
	 * @param aText
	 */
	public void setAText(String aText) {
		AText = aText;
	}
	
	/**
	 * Gets the A ID for that Story ID set.
	 * @return
	 */
	public int getAID() {
		return AID;
	}
	/**
	 * Sets the A ID for that Story ID set.
	 * @param aID
	 */
	public void setAID(int aID) {
		AID = aID;
	}
	/**
	 * Gets the B text for that Story ID set.
	 * @return
	 */
	public String getBText() {
		return BText;
	}
	/**
	 * Sets the B text for that Story ID set.
	 * @param bText
	 */
	public void setBText(String bText) {
		BText = bText;
	}
	/**
	 * Gets the B ID for that Story set.
	 * @return
	 */
	public int getBID() {
		return BID;
	}
	
	/**
	 * Sets the B ID for that Story set.
	 * @param bID
	 */
	public void setBID(int bID) {
		BID = bID;
	}
	
	/**
	 * Gets the C text for that Story set.
	 * @return
	 */
	public String getCText() {
		return CText;
	}
	
	/**
	 * Sets the C text for that Story set.
	 * @param cText
	 */
	public void setCText(String cText) {
		CText = cText;
	}
	
	/**
	 * Gets the C ID for that Story set.
	 * @return
	 */
	public int getCID() {
		return CID;
	}
	
	/**
	 * Sets the C ID for that Story set.
	 * @param cID
	 */
	public void setCID(int cID) {
		CID = cID;
	}
	
	/**
	 * Gets the D text for that Story set.
	 * @return
	 */
	public String getDText() {
		return DText;
	}
	
	/**
	 * Sets the D text for that Story set.
	 * @param dText
	 */
	public void setDText(String dText) {
		DText = dText;
	}
	
	/**
	 * Gets the D ID for that Story set.
	 * @return
	 */
	public int getDID() {
		return DID;
	}
	
	/**
	 * Sets the D ID for that Story set.
	 * @param dID
	 */
	public void setDID(int dID) {
		DID = dID;
	}

}
