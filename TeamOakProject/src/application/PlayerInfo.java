package application;

/**
 * PlayerInfo
 * Created April 8, 2021
 */
public class PlayerInfo  {
	/**
	 * id is given sequentially to each winner: id is ordered by completion, with newer dates at the bottom.
	 */
	private String id;
	/**
	 * dateCompleted is read from PlayerInfo.db by the 'date' column using the CURRENT_DATE function in SQLite. This is added by GameScreenController if user triggers win condition.
	 */
	private String dateCompleted;
	/**
	 * name is read from PlayerInfo.db by 'name' column. This is added to the database by GameScreenController using charName String if user triggers win condition.
	 */
	private String name;
	/**
	 * role is read from PlayerInfo.db by 'role' column. This is added to the database by GameScreenController using gender String if user triggers win condition.
	 */
	private String role;
	
	/**
	 * Constructor setting parameters for id, dateCompleted, name and role
	 * @param id
	 * @param dateCompleted
	 * @param name
	 * @param role
	 */
	public PlayerInfo(String id, String dateCompleted, String name, String role) {
		super();
		this.id = id;
		this.dateCompleted = dateCompleted;
		this.name = name;
		this.role = role;
	}
	/**
	 * @return the entry's id.
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id of this winner.
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the date completed.
	 */
	public String getDateCompleted() {
		return dateCompleted;
	}

	/**
	 * Sets the date completed for this winner.
	 * @param dateCompleted
	 */
	public void setDateCompleted(String dateCompleted) {
		this.dateCompleted = dateCompleted;
	}

	/**
	 * @return the name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * sets the name for this winner.
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the title determined by gender.
	 */
	public String getRole() {
		return role;
	}
	/**
	 * sets the title determined by gender for this winner.
	 * @param role
	 */
	public void setRole(String role) {
		this.role = role;
	}
	
}
