package application;

import java.sql.*;

import javafx.collections.FXCollections;

import javafx.collections.ObservableList;
/**
 * PlayerInfoModel
 * Created on April 8, 2021 
 */
public class PlayerInfoModel {
	/**
	 * Tracks database changes as win conditions are triggered and winners are added via GameScreenController.
	 */
	private ObservableList<PlayerInfo> playerInfo;
	
	/**
	 * Queries from PlayerInfo table in PlayerInfo.db until it reaches the last ID.
	 */
	public PlayerInfoModel() {
		playerInfo = FXCollections.observableArrayList();
		
		Connection connection = null;
		
		try {
			connection = DriverManager.getConnection("jdbc:sqlite:resources/PlayerInfo.db");
			String pullString = "SELECT * FROM PlayerInfo";
			PreparedStatement ps = connection.prepareStatement(pullString);
			ResultSet rs = ps.executeQuery();
			
			
			while(rs.next()) {
				playerInfo.add(new PlayerInfo(rs.getString("id"), rs.getString("date"), 
						rs.getString("name"), rs.getString("role")));
			}	
			
		}
		catch(SQLException e) {
				e.printStackTrace();
		}
		
	}

	/**
	 * @return the Player data.
	 */
	public ObservableList<PlayerInfo> getPlayerInfo() {
		return playerInfo;
	}

	/**
	 * Sets the player data.
	 * @param playerInfo
	 */
	public void setPlayerInfo(ObservableList<PlayerInfo> playerInfo) {
		this.playerInfo = playerInfo;
	}
	
	

}
