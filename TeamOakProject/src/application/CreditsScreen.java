package application;
	
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.BorderPane;
import javafx.fxml.FXMLLoader;

/**
 * CreditsScreen
 * Created on April 15, 2021
 */
public class CreditsScreen extends Application {
	@Override
	/**
	 * Sets dimensions of application window
	 * Retrieves CreditsScreen.fxml for styling and arrangement.
	 */
	public void start(Stage primaryStage) {
		try {
			BorderPane root = (BorderPane)FXMLLoader.load(getClass().getResource("CreditsScreen.fxml"));
			Scene scene = new Scene(root,800,600);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Launches application starting at CreditsScreen screen.
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
	}
}
