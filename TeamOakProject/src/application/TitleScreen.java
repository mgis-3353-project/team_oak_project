package application;
	
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.fxml.FXMLLoader;

/**
 * TitleScreen
 * Created on March 25, 2021
 *
 */
public class TitleScreen extends Application {
	/**
	 * Sets dimensions of application window. 
	 * Disables resizing of application window on all screens.
	 * Retrieves TitleScreenProject.fxml for elements and styling.
	 */
	@Override
	public void start(Stage primaryStage) {
		try {
			AnchorPane root = (AnchorPane)FXMLLoader.load(getClass().getResource("TitleScreenProject.fxml"));
			Scene scene = new Scene(root,800,600);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.setResizable(false);
			primaryStage.show();
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Launches application starting at TitleScreen.
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
	}
}
