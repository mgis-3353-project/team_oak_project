package application;


import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;

/**
 * CredtisScreenController
 * Created on April 15, 2021
 * 
 *
 */
public class CredtisScreenController {
	/**
	 * On clicking, takes user to Title Screen
	 */
    @FXML
    private Button BackButton;

    /**
     * When user clicks on BackButton button, TitleScreenProject.fxml is retrieved and loaded.
     * Switches user to TitleScreen.
     * @param event
     * @throws IOException
     */
    @FXML
    void BackAction(ActionEvent event) throws IOException {
       	FXMLLoader loader = new FXMLLoader();
    	loader.setLocation(getClass().getResource("TitleScreenProject.fxml"));
    	Parent parent = loader.load();
    	Scene scene = new Scene(parent);
    	Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
    	window.setScene(scene);
    	window.show();

    }
	
}
