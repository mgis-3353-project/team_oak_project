package application;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.*;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.DriverManager;
import java.sql.SQLException;


import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.Stage;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;

/**
 * GameScreenController
 * Created on March 25, 2021
 */
public class GameScreenController {
	/**
	 * Creates an ObjectMapper object. 
	 */
	ObjectMapper mapper = new ObjectMapper();
	
	/**
	 * Initializes an Array of Story objects.
	 */
	Story[] story;
	
	/**
	 * Each set of choices is saved under a number.
	 */
	int ChoiceID;
	
	/**
	 * gender is retrieved from gender String in CharCreateController by GameScreenController controller.
	 * gender String reflects whether the user clicks mascButton("Warrior") or femButton("Shieldmaiden).
	 */
    private String gender;
    /**
     * charName is retrieved from charName String in CharCreateController by GameScreenController controller.
     * charName String is read from nameField text field in CharCreateController.java
     */
    private String charName;
    
    /**
     * @param gender is set by user clicking mascButton or femButton in CharCreateController.
     * @param charName is set by reading nameField text field in CharCreateController.
     * A different version of the story is presented by each of the genders: "Story.json" for Warrior, "Story2.json" for Shieldmaiden
     */
    public void initData(String gender, String charName) {
    	this.gender = gender;
    	this.charName = charName;
    	
    	if (gender.equals("Warrior") || gender == null) {
			try {
				story = mapper.readValue(new File("Story.json"), Story[].class);
				
				dynamicText.setText(story[0].getStoryText());
				buttonA.setText(story[0].getAText());
				buttonB.setText(story[0].getBText());
				buttonC.setText(story[0].getCText());
				buttonNext.setText(story[0].getDText());
				
			} catch (JsonParseException e) {
				
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
    	
    	else if (gender.equals("Shieldmaiden")) {
			try {
				story = mapper.readValue(new File("Story2.json"), Story[].class);
				
				dynamicText.setText(story[0].getStoryText());
				buttonA.setText(story[0].getAText());
				buttonB.setText(story[0].getBText());
				buttonC.setText(story[0].getCText());
				buttonNext.setText(story[0].getDText());
				
			} catch (JsonParseException e) {
				
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	else {
    		try {
				story = mapper.readValue(new File("StoryF.json"), Story[].class);
				
				dynamicText.setText(story[0].getStoryText());
				buttonA.setText(story[0].getAText());
				buttonB.setText(story[0].getBText());
				buttonC.setText(story[0].getCText());
				buttonNext.setText(story[0].getDText());
				
			} catch (JsonParseException e) {
				
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	
    	
    }

    /**
     * Unused area meant for images corresponding to specific points in the story.
     */
    @FXML
    private ImageView image;

    /**
     * retrieves text from getAText()
     */
    @FXML
    private Button buttonA;

    /**
     * retrieves text from getBText()
     */
    @FXML
    private Button buttonB;

    /**
     * retrieves text from getCText()
     */
    @FXML
    private Button buttonC;

    /**
     * retrieves text from getDText()
     */
    @FXML
    private Button buttonNext;

    /**
     * returns user to Title Screen.
     */
    @FXML
    private Button quit;
    
    /**
     * Shows the text displayed under StoryText in Story.json or Story2.json. Displays sequentially.
     */
    @FXML
    private Text dynamicText;
    
    /**
     * Container for dynamicText.
     */
    @FXML
    private TextFlow gameplayText;

    /**
     * When user clicks on the 'A' button, the 'A' ID for that set of choices is retrieved and set as the current ChoiceID for that set.
     * Story text and A-C and Next button texts read the text listed under their corresponding texts for that id.
     * @param event
     */
    @FXML
    void aEvent(ActionEvent event) {
    
    	ChoiceID = story[ChoiceID].getAID();
        
    	dynamicText.setText(story[ChoiceID].getStoryText());
    	
    	buttonA.setText(story[ChoiceID].getAText());
    	buttonB.setText(story[ChoiceID].getBText());
    	buttonC.setText(story[ChoiceID].getCText());
    	buttonNext.setText(story[ChoiceID].getDText());
    	
    	
    
    }

    /**
     * When user clicks on the 'B' button, the 'B' ID for that set of choices is retrieved and set as the current ChoiceID for that set.
     * Story text and A-C and Next button texts read the text listed under their corresponding texts for that id.
     * @param event
     */
    @FXML
    void bEvent(ActionEvent event) {
    	
    	ChoiceID = story[ChoiceID].getBID();
        
    	dynamicText.setText(story[ChoiceID].getStoryText());
    	
    	buttonA.setText(story[ChoiceID].getAText());
    	buttonB.setText(story[ChoiceID].getBText());
    	buttonC.setText(story[ChoiceID].getCText());
    	buttonNext.setText(story[ChoiceID].getDText());
    
    }
    
    /**
     * When user clicks on the 'C' button, the 'C' ID for that set of choices is retrieved and set as the current ChoiceID for that set.
     * Story text and A-C and Next button texts read the text listed under their corresponding texts for that id.
     * @param event
     */
    @FXML
    void cEvent(ActionEvent event) {
    	ChoiceID = story[ChoiceID].getCID();
        
    	dynamicText.setText(story[ChoiceID].getStoryText());
    	
    	buttonA.setText(story[ChoiceID].getAText());
    	buttonB.setText(story[ChoiceID].getBText());
    	buttonC.setText(story[ChoiceID].getCText());
    	buttonNext.setText(story[ChoiceID].getDText());
    
    }
    /**
     * When user clicks on the 'D' button, the 'D' ID for that set of choices is retrieved and set as the current ChoiceID for that set.
     * Story text and A-C and D button texts read the text listed under their corresponding texts for that id.
     * If IDs 12 or 16 are listed under the 'D' ID, the user has triggered the win condition and their name, gender/title, and date are recorded in PlayerInfo.db.
     * @param event
     */
    @FXML
    void NextEvent(ActionEvent event) throws IOException {
    	
    	if (story[ChoiceID].getId() == 12 || story[ChoiceID].getId() == 16  ) {
    		try(Connection connection = DriverManager.getConnection("jdbc:sqlite:resources/PlayerInfo.db"))
    	    {    	    	
    		 	Statement statement = connection.createStatement();
    		 	
    		 	statement.executeUpdate("insert into PlayerInfo(date, name, role) values(CURRENT_DATE, '" + charName + "' , '" + gender + "')");
    	    }
    	    
    	    catch(SQLException e)
    	    {
    	    	System.out.printf("SQL ERROR:  %s%n", e.getMessage());
    	    }
    	   	FXMLLoader loader = new FXMLLoader();
        	loader.setLocation(getClass().getResource("PlayerInfo.fxml"));

        	
        	Parent parent = loader.load();
        	Scene scene = new Scene(parent);
        	Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        	window.setScene(scene);
        	window.show();
    	}
    	
    	else {
    	
    		
    	ChoiceID = story[ChoiceID].getDID();
        
    	dynamicText.setText(story[ChoiceID].getStoryText());
    	
    	buttonA.setText(story[ChoiceID].getAText());
    	buttonB.setText(story[ChoiceID].getBText());
    	buttonC.setText(story[ChoiceID].getCText());
    	buttonNext.setText(story[ChoiceID].getDText());
    	}
      
    	
    }

    /**
     * Returns user to Title Screen.
     * Retrieves and loads TitleScreenProject.fxml
     * @param event
     * @throws IOException
     */
    @FXML
    void quitEvent(ActionEvent event) throws IOException{
    	FXMLLoader loader = new FXMLLoader();
    	loader.setLocation(getClass().getResource("TitleScreenProject.fxml"));
    	Parent parent = loader.load();
    	Scene scene = new Scene(parent);
    	Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
    	window.setScene(scene);
    	window.show();
    }
    
    /**
     * Resets ChoiceID to zero.
     */
    public void initialize() {
    	
    	ChoiceID = 0;
    	
    }

}
